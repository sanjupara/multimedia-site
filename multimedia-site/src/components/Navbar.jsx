import React from 'react';
import { Link } from 'react-router-dom';

export function Navbar () {
  return (
    <ul>
      <li><Link to="/Startpage">Home</Link></li>
      <li><Link to="/Offer">Offer</Link></li>
      <li><Link to="/About">About</Link></li>
      <li><Link to="/Contact">Contact</Link></li>
      <li><Link to="/Impressum">Impressum</Link></li>
    </ul>
  );
}