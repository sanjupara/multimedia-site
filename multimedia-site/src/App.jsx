import './index.css';
import './App.css';
import {Startpage} from './pages/Startpage';
import {About} from './pages/About';
import {Contact} from './pages/Contact';
import {Offer} from './pages/Offer';
import {Impressum} from './pages/Impressum';
import { BrowserRouter, Routes, Route } from "react-router-dom"
import { Navbar } from './components/Navbar';


function App() {

  return (   
      <div>
        <BrowserRouter>
          <Navbar />
            <Routes>
              <Route path="/" element={<Startpage />} />
              <Route path="/Startpage" element={<Startpage />} />
              <Route path="/About" element={<About />} />
              <Route path="/Contact" element={<Contact />} />
              <Route path="/Offer" element={<Offer />} />
              <Route path="/Impressum" element={<Impressum />} />
              <Route path="*" element={<h1>404: Not Found</h1>} />
            </Routes>
        </BrowserRouter>  
      </div>
      
  );
}

export default App;
