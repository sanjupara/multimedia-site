import React from "react";
import '../css/Startpage.css';
import '../css/About.css'
import image1 from '../images/image1.jpg';
import { Offer } from './Offer';
import Post0 from '../images/Post0.jpg';
import Post2 from '../images/Post2.jpg';
import sanan from '../images/sanan.jpg'
import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Parallax, ParallaxLayer } from '@react-spring/parallax';


export function About() {
    return(        
        <div>
            <Parallax pages={2.5} factor={1}>
                
                <ParallaxLayer offset={0} speed={1}>
                    <h1>
                        Wir sind die Sancert Gmbh
                    </h1>
                </ParallaxLayer>
                    
                <ParallaxLayer offset={1} speed={0.5}>
                    <div className="backgroundbox">
                        <p>
                        Welcome to Sancert GmbH, your premier concert organizer in Zürich, Switzerland. We specialize in capturing unforgettable moments through stunning concert photography. From intimate acoustic performances to large-scale shows, we are dedicated to preserving the magic of live music through captivating visuals. Join us as we bring the power of music to life through the lens.
                        </p>
                    </div>
                </ParallaxLayer>

                <ParallaxLayer offset={1.5} speed={0.5}>
                    <div>
                        <h2>
                            Our Founder
                        </h2>
                    </div>
                    <div className="sanan">
                        <img src={sanan} alt="" />
                    </div>
                </ParallaxLayer>

                
            </Parallax>
        </div>
        
    );
};

