import React from "react";
import '../css/Startpage.css';
import '../css/Offer.css';
import image1 from '../images/image1.jpg';
import  SOS  from '../images/SOS.png'
import DAMN from '../images/Damn.jpg'
import HeavenHell from '../images/HeavenHell.png'


export function Offer() {
    return(        
        <div className="tile-container">
            <div className="tile">
                <h3>SZA</h3>
                <img src={SOS} alt="" />
                <p>
                    06.06.2023
                </p>
                <button>
                    Not Available
                </button>
            </div>
            <div className="tile">
                <h3>Kendrick Lamar</h3>
                <img src={DAMN} alt="" />
                <p>
                    10.10.2022
                </p>
                <button>
                    Not Available
                </button>
            </div>
            <div className="tile">
                <h3>Don Toliver</h3>
                <img src={HeavenHell} alt="" />
                <p>
                    10.10.2023
                </p>
                <button>
                    Buy
                </button>
            </div>
        </div>
    );
};