import React from "react";
import '../css/Startpage.css';
import '../css/Contact.css'
import image1 from '../images/image1.jpg';

export function Contact() {
    return(        
        <div>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2700.036958385251!2d8.549505915623726!3d47.41122027917223!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47900a82487bbd17%3A0x11cbbd4097290cc9!2sHallenstadion%20Z%C3%BCrich!5e0!3m2!1sde!2sch!4v1688110405340!5m2!1sde!2sch"
                width="600"
                height="450"
                style={{ border: 0 }}
                allowFullScreen=""
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
                frameBorder="0"
                title="Map"
            ></iframe>
            <div className="contact-form-container">
                <h2>Contact Us</h2>
                <h3>sananjayan@hotmail.com</h3>
                <h3>Wallisellenstrasse 17a</h3>
            </div>
        </div>
    );
};