import { Carousel } from 'react-responsive-carousel';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Parallax, ParallaxLayer } from '@react-spring/parallax';
import React from "react";
import '../css/Offer.css';
import image1 from '../images/image1.jpg';
import Post0 from '../images/Post0.jpg';
import Post2 from '../images/Post2.jpg';
import Kendrick from '../images/KendrickKeem1.jpg'
import Kendrick2 from '../images/KendrickPianoOriginal.jpg'



export function Startpage() {
    return(        
        <div>
            <Parallax pages={4} factor={1} style={{backgroundImage: `url(${image1})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat'}}>
                
                <ParallaxLayer offset={0.2} speed={1}>
                    <h1>
                        SANCERT GMBH
                    </h1>
                </ParallaxLayer>
                    
                <ParallaxLayer offset={1} speed={0.5}>
                    <div id='textbox'>
                        <h2>
                            Ihre Lieblingskünster Hautnah erleben
                        </h2>
                        <p>
                            SZA, Kendrick Lamar, Don Toliver and more!
                        </p>
                        <br />
                        <div className="imagegrid">
                            <img src={Post0} alt="" />
                            <img src={Post2} alt="" />
                        </div>
                    </div>
                </ParallaxLayer>

                <ParallaxLayer offset={2} speed={0.5} >
                    <div className="textbox">
                        <Carousel>
                            <div>
                                <img src={Kendrick2} alt="" />
                            </div>
                            <div>
                                <img src={Kendrick} alt="" />
                            </div>
                        </Carousel>
                    </div>
                </ParallaxLayer>
            </Parallax>
        </div>
    );
};

